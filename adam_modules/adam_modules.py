'''
Module for setting up a Modbus communication with minimalmodbus to Adam-4000 and Adam-4100 modules
'''
import minimalmodbus

class DummySerial(object):
    def flushOutput(self):
        pass
    def flushInput(self):
        pass
    def write(self, message):
        pass
    def read(self):
        return b'Blaah'

class DummyModbus(object):
    def __init__(self, portname, slaveaddress):
        self.serial = DummySerial()
    def read_register(self, channel):
        return None
    def read_registers(self, channel, number_of_channels):
        return list()
    def write_register(self, channel, value):
        pass
    def write_bit(self, channel, value):
        pass
    def read_bit(self, channel):
        return None



USE_DUMMY_MODBUS = False

if USE_DUMMY_MODBUS:
    minimalmodbus.Instrument = DummyModbus


class AdamModule(minimalmodbus.Instrument):
    name = None

    def __init__(self, portname, slaveaddress):
        minimalmodbus.Instrument.__init__(self, portname, slaveaddress)
    def get_name(self):
        return self.read_register(210)
    def get_version(self):
        return self.read_register(212)
    def is_correct_module(self):
        if self.name == self.get_name():
            return True
        else:
            return False
    def is_valid_channel(self, channel, number_of_channels):
        try:
            int(channel)
        except ValueError:
            print('Channel input not an integer')
            return False

        if channel >= 0 and channel <= number_of_channels:
            return True
        else:
            print('Channel input outside available channels: [0, ' + str(number_of_channels) + ']')
            return False


class AnalogIn(AdamModule):
    analog_in_start_channel = 1
    type_analog_in_start_channel = 201
    burn_out_signal_start_channel = 1
    analog_in_number_of_channels = 8

    def get_analog_in(self, channel=-1):
        if channel == -1:
            return self.read_registers(self.analog_in_start_channel - 1, self.analog_in_number_of_channels)
        elif self.is_valid_channel(channel, self.analog_in_number_of_channels):
            return self.read_register(self.analog_in_start_channel - 1 + channel)
        else:
            print('Channel out of range')
        
    def set_type_analog_in(self, channel, value):
        return self.write_register(self.type_analog_in_start_channel - 1 + channel, value)
    def get_type_analog_in(self, channel=-1):
        if channel == -1:
            return self.read_registers(self.type_analog_in_start_channel - 1, self.analog_in_number_of_channels)
        elif self.is_valid_channel(channel, self.analog_in_number_of_channels):
            return self.read_register(self.type_analog_in_start_channel - 1 + channel)
        else:
            print('Channel out of range')

    def get_burn_out_signal(self, channel):
        return self.read_bit(self.burn_out_signal_start_channel - 1 + channel)

class AnalogOut(AdamModule):
    analog_out_start_channel = 1
    type_analog_out_start_channel = 201
    analog_out_number_of_channels = 8

    def set_analog_out(self, channel, value):
        return self.write_register(self.analog_out_start_channel - 1 + channel, value)
    def get_analog_out(self, channel=-1):
        if channel == -1:
            return self.read_registers(self.analog_out_start_channel - 1, self.analog_out_number_of_channels)
        elif self.is_valid_channel(channel, self.analog_out_number_of_channels):
            return self.read_register(self.analog_out_start_channel - 1 + channel)
        else:
            print('Channel out of range')
    def set_type_analog_out(self, channel, value):
        return self.read_register(self.analog_out_start_channel - 1 + channel, value)
    def get_type_analog_out(self, channel=False):
        if channel == -1:
            return self.read_registers(self.analog_out_start_channel - 1, self.analog_out_number_of_channels)
        elif self.is_valid_channel(channel, self.analog_out_number_of_channels):
            return self.read_register(self.analog_out_start_channel - 1 + channel)
        else:
            print('Channel out of range')

class DigitalIn(AdamModule):
    diginal_in_start_channel = 1
    digital_in_number_of_channels = 8

    def get_digital_in(self, channel):
        return self.read_bit(self.diginal_in_start_channel - 1 + channel)

class DigitalOut(AdamModule):
    digital_out_start_channel = 17
    digital_out_number_of_channels = 8

    def set_digital_out(self, channel, value):
        return self.write_bit(self.digital_out_start_channel - 1 + channel, value)
    def get_digital_out(self, channel):
        return self.read_bit(self.digital_out_start_channel - 1 + channel)


class Adam4117(AnalogIn):
    """
    Adam-4117
    """
    name = 16663
    analog_in_start_channel = 1
    type_analog_in_start_channel = 201
    burn_out_signal_start_channel = 1
    analog_in_number_of_channels = 8

    # Type       Code
    # +/- 100mV: 2
    # +/-500 mV: 3
    # +/-1V:     4
    # +/- 2,5V:  5
    # 4~20mA:    7
    # +/-10V:    8
    # +/-5V:     9
    # 0~20 mA:   13
    # K:         15
    # T:         16
    # E:         17
    # R:         18
    # S:         19
    # B:         20
    # J:         14


class Adam4019(AnalogIn):
    """
    ADAM4019
    """
    name = None
    analog_in_start_channel = 1
    type_analog_in_start_channel = 201
    burn_out_signal_start_channel = 201
    analog_in_number_of_channels = 8


class Adam4024(AnalogOut, DigitalIn):
    """
    ADAM4024
    """
    name = None
    analog_in_start_channel = 1
    type_analog_in_start_channel = 201
    burn_out_signal_start_channel = 201
    analog_in_number_of_channels = 4
    diginal_in_start_channel = 1
    digital_in_number_of_channels = 4

    # Analog out signal range is 0 to 409[5 or 6]
    # should scale such that value goes from 0 to 1

    # Types
    # 48: 0~20 mA
    # 49: +/- 10V
    # 50: 4~20 mA


class Adam4055(DigitalIn, DigitalOut):
    """
    ADAM4055
    """
    name = None
    digital_out_start_channel = 17
    digital_out_number_of_channels = 8
    diginal_in_start_channel = 1
    digital_in_number_of_channels = 8


class Adam4069(DigitalOut):
    """
    ADAM4069
    """
    name = None
    digital_out_start_channel = 17
    digital_out_number_of_channels = 8
