============
Adam Modules
============

adam_madules provides an interface for Advantech ADAM modules using modbus protocol
often looks like this::

    #!/usr/bin/env python

    from adam_modules.adam_modules import Adam4017

    sensor = Adam4017(2, 1)  # port, address
    sensor.get_analog_in(2)  # channel number


Installation
============

using pip::

	pip install -e git+https://bitbucket.org/sysengntnu/adam-modules#egg=adam_module