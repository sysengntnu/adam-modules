from setuptools import setup, find_packages
from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))

setup(
	name='adam_modules',
	version='0.2.1.dev',
	author='Chriss Grimholt',
    author_email='grimholt@me.com',
	description='Interface for the Advantech ADAM modules using modbus protocol',
	url='https://bitbucket.org/sysengntnu/adam-modules.git',
    packages=['adam_modules', 'adam_modules.utils'],
	install_requires = ['minimalmodbus']
	)

#scripts=['bin/stowe-towels.py','bin/wash-tow']
